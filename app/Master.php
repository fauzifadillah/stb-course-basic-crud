<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
  protected $fillable = [

    'share_name_stock',
    'share_price_stock',
    'share_qty_stock',
    'total'  ];
}
