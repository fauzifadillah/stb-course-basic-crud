<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Share;
use App\Master;
use DB;

class ShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $shares = Share::all();
      $masters = Master::all();


    $shares_ttl = DB::select('select *, sum(share_qty) as ttl_qty, sum(share_price) as ttl_price, (share_price * share_qty) as jml from shares group by share_name');




      return view('shares.index', compact('shares','shares_ttl','masters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'share_name'=>'required',
        'share_price'=> 'required|integer',
        'share_qty' => 'required|integer'
      ]);
      $share = new Share([
        'kode_barang' => $request->get(''),
        'share_name' => $request->get('share_name'),
        'share_price'=> $request->get('share_price'),
        'share_qty'=> $request->get('share_qty')

      ]);
      $share->save();

      $master = Master::where('share_name_stock',$request->get('share_name'))->First();
      $master->share_qty_stock = $master->share_qty_stock - $request->get('share_qty');

      $master->save();

      return redirect('/shares')->with('success', 'Barang berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $share = Share::find($id);

        return view('shares.edit', compact('share'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
     'share_name'=>'required',
     'share_price'=> 'required|integer',
     'share_qty' => 'required|integer'
   ]);

   $share = Share::findOrFail($id);
   $share->share_name = $request->get('share_name');
   $share->share_price = $request->get('share_price');
   $share->share_qty = $request->get('share_qty');
   $share->save();

   return redirect('/shares')->with('success', 'Barang berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $share = Share::find($id);
            $master = Master::where('share_name_stock',$share->share_name)->First();

      $master->share_qty_stock = $master->share_qty_stock + $share->share_qty;
      $master->save();
      $share->delete();



      return redirect('/shares')->with('success', 'Barang berhasil dihapus');
    }
}
