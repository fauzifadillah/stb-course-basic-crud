<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Share;
use App\Master;
use DB;

class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


      $masters = Master::all();

      return view('masters.master_index', compact('masters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masters.master_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'share_name_stock'=>'required',
        'share_price_stock'=> 'required|integer',
        'share_qty_stock' => 'required|integer'
      ]);
      $master = new Master([
        'share_name_stock' => $request->get('share_name_stock'),
        'share_price_stock'=> $request->get('share_price_stock'),
        'share_qty_stock'=> $request->get('share_qty_stock')

      ]);
      $master->save();

      
      return redirect('/masters')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $masters = Master::find($id);

        return view('masters.master_edit', compact('masters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
     'share_name_stock'=>'required',
     'share_price_stock'=> 'required|integer',
     'share_qty_stock' => 'required|integer'
   ]);

   $master = Master::findOrFail($id);
   $master->share_name_stock = $request->get('share_name_stock');
   $master->share_price_stock = $request->get('share_price_stock');
   $master->share_qty_stock = $request->get('share_qty_stock');
   $master->save();

   return redirect('/masters')->with('success', 'Stock has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $master = Master::find($id);
      $master->delete();

      return redirect('/masters')->with('success', 'Stock has been deleted Successfully');
    }
}
