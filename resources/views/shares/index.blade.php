@extends('test')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="jumbotron text-center">
  <h1> <kbd>Contoh CRUD </kbd></h1>
</div>
<button type="button" class="btn btn-primary btn-md" data-toggle="modal"  data-target="#ModalCreate">
    POPUP CREATE
</button>



<!-- Modal HTML Markup -->

<div id="ModalCreate" class="modal fade">
   <div class="modal-dialog" role="document">
     <div class="modal-header" >

        </div>
       <div class="modal-content">
         <div class="card uper">
           <div class="card-header">
             FORM CREATE
           </div>
           <div class="card-body">
             @if ($errors->any())
               <div class="alert alert-danger">
                 <ul>
                     @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                     @endforeach
                 </ul>
               </div><br />
             @endif
               <form method="post" action="{{ route('shares.store') }}">
                 @csrf
                    <!-- Nama -->
                   <div class="form-group">
                       <label for="name">Nama:</label>
                       <input type="text" class="form-control" name="share_name" placeholder="Nama"/>
                   </div>
                    <!-- Harga -->
                   <div class="form-group">
                       <label for="price">Harga :</label>
                       <input type="text" class="form-control" name="share_price" placeholder="Harga" >
                   </div>
                    <!-- Kuantitas -->
                   <div class="form-group">
                       <label for="quantity">Kuantitas:</label>
                       <input type="text" class="form-control" name="share_qty" placeholder="Kuantitas" >


                   </div>
                   <button type="submit" class="btn btn-primary">Add</button>
               </form>


           </div>
         </div>

       </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" ></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css">

<a href="{{ route('shares.create')}}" class="btn btn-danger ">CREATE</a></td>

<div class="uper">
  <table class="table table-bordered">
    <thead>
      <tr>

        <td>Nama Barang</td>
        <td>Harga Barang</td>
        <td>Jumlah Barang Tersedia</td>
      </tr>
    </thead>
    <tbody>
      @foreach($masters as $m)
      <tr>

          <td>{{$m->share_name_stock}}</td>
          <td>Rp.{{$m->share_price_stock}}</td>
          <td>{{$m->share_qty_stock}}</td>



      </tr>
      @endforeach
    </tbody>

<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif

  <table class="table table-bordered">
    <thead>
        <tr>

          <td>Nama Barang</td>
          <td>Harga Barang</td>
          <td>Jumlah Barang</td>
          <td>Sub total Harga</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
<h3>Transaksi</h3>
    <tbody>
        @foreach($shares as $share)
        <tr>



            <td>{{$share->share_name}}</td>
            <td>Rp.{{$share->share_price}}</td>
            <td>{{$share->share_qty}}</td>
            <td>Rp.{{$share->share_qty * $share->share_price}}</td>
            <td><a href="{{ route('shares.edit',$share->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('shares.destroy', $share->id )}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>



<h3>Summary</h3>
  <div class="uper">

    <table class="table table-bordered">
      <thead>
          <tr>
            <td> Nama Barang </td>
            <td>Sub Total Barang</td>
            <td>Sub Total Harga</td>
            <td>Total</td>

          </tr>
      </thead>
      <tbody>
          @foreach($shares_ttl as $ttl)
          <tr>
              <td>{{$ttl->share_name}}</td>
              <td>{{$ttl->ttl_qty}}</td>
              <td>Rp.{{$ttl->ttl_price}}</td>
              <td>Rp.{{$ttl->ttl_qty * $ttl->ttl_price}}</td>

          </tr>
          @endforeach
          <table class="table table-bordered">
            <thead>

                <tr>
                  <td>  </td>
                </tr>

            </thead>


      </tbody>
    </table>
  <div>
@endsection
