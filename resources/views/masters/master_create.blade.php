@extends('test')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('masters.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Nama Barang *master:</label>
              <input type="text" class="form-control" name="share_name_stock"/>
          </div>
          <div class="form-group">
              <label for="price">Harga Barang *master :</label>
              <input type="text" class="form-control" name="share_price_stock"/>
          </div>
          <div class="form-group">
              <label for="quantity">Kuantitas Barang *master :</label>
              <input type="text" class="form-control" name="share_qty_stock"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@endsection
